package net.pl3x.pl3xmotd.commands;

import net.pl3x.pl3xmotd.Pl3xMOTD;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CmdPl3xMOTD implements CommandExecutor {
	private Pl3xMOTD plugin;

	public CmdPl3xMOTD(final Pl3xMOTD plugin) {
		this.plugin = plugin;
	}

	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if (!cmd.getName().equalsIgnoreCase("pl3xmotd"))
			return false;
		if (!cs.hasPermission("pl3xmotd.command.pl3xmotd")) {
			cs.sendMessage(plugin.colorize("&4You do not have permission for that command!"));
			plugin.log(cs.getName() + " was denied access to that command!");
			return true;
		}
		if (args.length > 0) {
			if (args[0].equalsIgnoreCase("reload")) {
				plugin.reloadConfig();
				cs.sendMessage(plugin.colorize("&d" + plugin.getName() + " config.yml has been reloaded."));
				if (plugin.getConfig().getBoolean("debug-mode", false))
					plugin.log(plugin.colorize("&aReloaded config.yml"));
				return true;
			}
		}
		cs.sendMessage(plugin.colorize("&d" + plugin.getName() + " v" + plugin.getDescription().getVersion()));
		return true;
	}
}
